jQuery(document).ready(function($) {
  $("a.btop").click(function() {
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	// Ajax Post Loading, Handled in pilot-theme-functions.php

	$('.load-more').on( 'click', function( event ) {
		event.preventDefault();
		var numPosts = $('article.post').length;

		$.ajax({
			url: ajax.ajaxurl,
			type: 'post',
			data: {
				action: 'ajax_pagination',
				offsetNum: numPosts
			},
			success: function( result ) {
				$('.post-container').append(result);
			}
		});
	});

	// Hamburger menu

	$('.navbar-toggle').on('click', function() {
		if ( !$('body').hasClass('menu-open') ) {
			$('body').addClass('menu-open');
			$('#myNavmenu').addClass('menu-open');
		} else {
			$('body').removeClass('menu-open');
			$('#myNavmenu').removeClass('menu-open');
		}
	});

	$('.btn-nav-close').on('click', function() {
		$('body').removeClass('menu-open');
		$('#myNavmenu').removeClass('menu-open');
	});
});