<?php get_header(); ?>

	<div class="container">
		<?php if ( have_posts() ) : ?>

			<header><h2 class="page-title">Blog</h2></header>

			<div class="post-container">
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'views/content', 'excerpt' ); ?>
				<?php endwhile; ?>
			</div>
			<button class="load-more">LOAD MORE</button>
		<?php else : ?>
			<?php get_template_part( 'views/content', 'none' ); ?>
		<?php endif; ?>
	</div>

<?php get_footer(); ?>