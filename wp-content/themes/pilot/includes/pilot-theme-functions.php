<?php
	function pilot_get_title(){
		if (is_home()) {
			if (get_option('page_for_posts', true)) {
				return get_the_title(get_option('page_for_posts', true));
			}
			else {
				return __('Latest Posts', 'dorado');
			}
		} elseif (is_archive()) {
			return get_the_archive_title();
		}
		elseif (is_search()) {
			return sprintf(__('Search Results for %s', 'dorado'), get_search_query());
		}
		elseif (is_404()) {
			return __('Not Found', 'dorado');
		}
		else {
			return get_the_title();
		}
	}
	function pilot_get_view_format(){
		return;
	}
	function pilot_get_sidebar(){
		global $pilot;
		if( $pilot->sidebar ){
			get_sidebar();
		}
	}
	function pilot_get_comments(){
		global $pilot;
		if( $pilot->comments ){
			if ( comments_open() || get_comments_number() ){
				comments_template();
			}			
		}
	}
	
	function asset_path($filename) {
		$dist_path = get_template_directory_uri() . DIST_DIR;
		$directory = dirname($filename) . '/';
		$file = basename($filename);
		static $manifest;
		
		if (empty($manifest)) {
			$manifest_path = get_template_directory() . DIST_DIR . 'assets.json';
			$manifest = new JsonManifest($manifest_path);
		}
		if (array_key_exists($file, $manifest->get())) {
			return $dist_path . $directory . $manifest->get().array($file);
		} else {
			return $dist_path . $directory . $file;
		}
	}

	//Edit 'More' Link

	function pilot_excerpt_string($more) {
		global $post;
		return '...<a class="read-more" href="' . get_permalink($post->ID) . '">READ</a>';
	}

	add_filter('excerpt_more', 'pilot_excerpt_string');


	// AJAX Pagination

	add_action( 'wp_ajax_nopriv_ajax_pagination', 'pilot_ajax_pagination' );
	add_action( 'wp_ajax_ajax_pagination', 'pilot_ajax_pagination' );

	function pilot_ajax_pagination() {
		$html;

		$args=array(
			'post_type' => 'post',
			'post_status' => 'publish',
			'posts_per_page' => 3,
			'paged' => '1',
			'offset' => $_POST['offsetNum']
		);
		$query = new WP_Query($args);

		if( $query->have_posts() ):
			foreach( $query->posts as $post ){

				setup_postdata($post);
				$post->permalink = get_permalink($post->ID);
				$thumb_url_array = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'large');
				$post->url = $thumb_url_array[0];
				$date = strtotime( $post->post_date );
				$post->pretty_date = date( 'd/m/Y', $date );
				$post->excerpt = get_the_excerpt($post->ID);
				$post->classes = implode( ' ', get_post_class('', $post->ID) );

				// Duplicate excerpt view to return as string
				$html = $html . '<article id="post-' . $post->ID . '"' . ' class="post ' . $post->classes . '">';

				if ( $post->url ) :
					$html = $html . '<div class="post-img" style="background-image: url(' . $post->url . ')"></div>';
				endif;

				$html = $html . '<header class="entry-header">
						<div class="entry-meta">
							<time class="entry-date" datetime="' . $post->pretty_date . '">' . $post->pretty_date . '</time>
						</div><!-- .entry-meta --><h3 class="entry-title"><a href="' . $post->permalink . '" >' . $post->post_title .'</a></h3></header><!-- .entry-header -->
					<div class="entry-summary">' . $post->excerpt . '
					</div><!-- .entry-summary -->
				</article><!-- #post-## -->';
			}
			echo $html;
		endif;
		die();
	}
?>