<header id="masthead" class="site-header">
	<nav id="myNavmenu" class="navmenu navmenu-inverse navmenu-fixed-right offcanvas in canvas-slid" role="navigation" >
	  <a href="javascript:void(0)" class="navbar-rside btn-nav-close">×</a>
	  
	  <ul class="nav navmenu-nav">
		  <li>
		    <a href="http://kushly.com/page/about-kushly">About Kushly</a>
		  </li>
		  <li>
		    <a href="http://kushly.com/page/our-dispensaries">Our Dispensaries</a>
		  </li>
		  <li>
		    <a href="http://kushly.com/page/become-a-partner">Become a Partner</a>
		  </li>
		  <li>
		    <a href="http://kushly.com/page/become-a-driver">Become a Driver</a>
		  </li>
		  <li>
		    <a href="http://kushly.com/page/how-kushly-work">How Kushly Work</a>
		  </li>
		  <li>
		    <a href="http://kushly.com/page/contact-kushly">Contact Kushly</a>
		  </li>
		  <li>
		    <a href="http://kushly.com/blog">Blogs</a>
		  </li>
			<!--<li>
			  <a href="/blog/medical-marijuana-blog">Marijuana News</a>
		  </li>-->
	  </ul>
	  <ul class="nav navmenu-nav">
	    <li><a href="http://kushly.com/signin">Sign-In</a></li>
	    <li><a href="http://kushly.com/signup">Sign-Up</a></li>
	  </ul>
	</nav>


  <nav class="navbar navbar-default navbar-fixed-top navbar-all">

	  <div class="navbar-header">
	    <a class="logo" href="<?php echo get_site_url(); ?>"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/image/logo.svg" width="113" height="42"></a>
	  </div>

		<ul class="navbar-right main-nav">
			<li>
	      <a href="javascript:void(0)" class="navbar-rside navbar-toggle" ><svg version="1.1" id="svg-bar" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 16 13" style="enable-background:new 0 0 16 13;" xml:space="preserve">
		      <g><g><g><path class="st0" d="M15.5,1h-15C0.2,1,0,0.8,0,0.5S0.2,0,0.5,0h15C15.7,0,16,0.2,16,0.5S15.7,1,15.5,1z"></path></g></g>
		      <g><g><path class="st0" d="M15.5,5h-15C0.2,5,0,4.8,0,4.5S0.2,4,0.5,4h15C15.7,4,16,4.2,16,4.5S15.7,5,15.5,5z"></path></g></g><g><g><path class="st0" d="M15.5,9h-15C0.2,9,0,8.8,0,8.5S0.2,8,0.5,8h15C15.7,8,16,8.2,16,8.5S15.7,9,15.5,9z"></path></g></g><g><g><path class="st0" d="M15.5,13h-15C0.2,13,0,12.8,0,12.5S0.2,12,0.5,12h15c0.3,0,0.5,0.2,0.5,0.5S15.7,13,15.5,13z"></path></g></g></g>
			    </svg>
			  </a>
		  </li>
	  </ul>
  </nav>
</header>