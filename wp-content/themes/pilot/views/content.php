<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
		  <div class="post-img" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
	<?php endif; ?>

	<div class="container">
		<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>
			<header class="entry-header">
				<?php
					if ( is_single() ) {
						the_title( '<h2 class="entry-title">', '</h2>' );
					} else {
						the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
					}
		
				if ( 'post' === get_post_type() ) : ?>
				<?php
				endif; ?>
				<?php if ( 'post' === get_post_type() ) : ?>
					<?php $date = get_the_date('d/m/Y'); ?>
					<div class="entry-meta">
						<time class="entry-date" datetime="<?php echo $date; ?>"><?php echo $date; ?></time>
					</div><!-- .entry-meta -->
				<?php endif; ?>
			</header><!-- .entry-header -->
		<?php endif; ?>
		<div class="entry-content">
			<?php
				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pilot' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
			?>
		</div><!-- .entry-content -->
	</div>

</article><!-- #post-## -->