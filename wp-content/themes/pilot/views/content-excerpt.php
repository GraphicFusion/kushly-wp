<?php global $pilot; ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
		  <div class="post-img" style="background-image: url('<?php the_post_thumbnail_url(); ?>')"></div>
	<?php endif; ?>
	<?php if( $pilot->use_default_page_titles && !get_field('hide_title') ) : ?>

		<header class="entry-header">
			<?php if ( 'post' === get_post_type() ) : ?>
				<?php $date = get_the_date('d/m/Y'); ?>
				<div class="entry-meta">
					<time class="entry-date" datetime="<?php echo $date; ?>"><?php echo $date; ?></time>
				</div><!-- .entry-meta -->
			<?php endif; ?>

			<?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" >', esc_url( get_permalink() ) ), '</a></h3>' ); ?>	
		</header><!-- .entry-header -->

	<?php endif; ?>
	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
</article><!-- #post-## -->
