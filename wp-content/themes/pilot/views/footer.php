<footer class="site-footer">
	<div class="footer-top">
    <a class="logo" href="<?php echo get_site_url(); ?>"><img src="<?php echo get_site_url(); ?>/wp-content/themes/pilot/image/logo.svg" width="113" height="42"></a>
	</div>
	<div class="bg-base">
    <div class="container">
      <div class="row credits">
        <p class="text-left">&copy; <?php echo date("Y"); ?> Kushly. All Rights Reserved. <a href="#x">Privacy Policy</a>, <a href="#x">Legal Terms</a>. Design by <a href="https://www.sonderagency.com">Sonder</a></p>
        <p class="text-right"><a class="btop" href="javascript:void(0)">Back to top</a></p>
      </div>
    </div>
  </div>
</footer>