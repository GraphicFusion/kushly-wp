<?php get_header(); ?>
<?php get_all_blocks(); ?>
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'views/content' ); ?>
		<?php endwhile; ?>

<?php get_footer(); ?>